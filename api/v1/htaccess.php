<?php
RewriteEngine Off # Turn off the rewriting engine
RewriteRule ^table/?$ table.php [NC,L]
RewriteRule ^table/([0-9]+)/?$ table.php?id=$1 [NC,L]
?>